import { Colors } from "../dto/colors"
import { Stats } from "../dto/stats"

const BN = require('bn.js')


class WordService {

    allStats:any[] 

    words = {
        '1': 'Theme',
        '2': 'Grass',
        '3': 'Dirt',
        '4': 'Diamond',
        '5': 'Bat',
        '6': 'Wood',
        '7': 'USA',

        '8': 'Font Face',
        '9': 'Arial',
        '10': 'Courier New',
        '11': 'Georgia',
        '12': 'Times New Roman',
        '13': 'Verdana',

        '14': 'Font Size',
        '15': '50',
        '16': '80',
        '17': '110',
        '18': '140',

        '19': 'Font Style',
        '20': 'NORMAL',
        '21': 'ITALIC',
        '22': 'BOLD',
        '23': 'BOLDITALIC'
    }

    getWordCssRule(background, colors) {

        if (background == "Dirt") {
            return `
        .word { 
            border: 5px solid ${colors.borderColor};
            background: var(--dirtSVG);
        }
        `
        }

        if (background == "Bat") {
            return `
        .word { 
            border: 5px solid ${colors.borderColor};
            background: var(--baseballBatSVG);
            background-size: 100% 100%;
        }
        `
        }


        return `
        .word { 
            border: 5px solid ${colors.borderColor};
            background-image: repeating-linear-gradient(180deg, ${colors.color1}, ${colors.color1} 30px, ${colors.color2} 30px, ${colors.color2} 60px);
        }
    `
    }

    getTextCssRule(background, colors) {
        return `
        main .word .text {
            background: ${colors.textBackground};
            color: ${colors.fontColor}; 
        }
    `
    }

    getColors(background): Colors {

        switch (background) {

            case "Grass":

                return {
                    color1: "#56ab2f",
                    color2: "#a8e063",
                    fontColor: '#000000',
                    borderColor: "#89C165",
                    textBackground: "#56ab2f"
                }

            case "Dirt":

                return {
                    color1: "#977451",
                    color2: "#E9BA8B",
                    fontColor: '#000000',
                    borderColor: "#906E4C",
                    textBackground: "#FFFFFF"
                }

            case "Diamond":

                return {
                    color1: "#6fdff8",
                    color2: "#a5edfe",
                    fontColor: '#000000',
                    borderColor: "#3a96dd",
                    textBackground: "#31d2f7"

                }

            case "Bat":

                return {
                    color1: "#edc800",
                    color2: "#e3b600",
                    fontColor: '#FFFFFF',
                    borderColor: "#000000",
                    textBackground: "#000000"
                }

            case "USA":

                return {
                    color1: '#b32134',
                    color2: '#ffffff',
                    fontColor: '#FFFFFF',
                    borderColor: '#3b3b6d',
                    textBackground: "#3b3b6d"
                }
        }

    }

    getAttributePairs(attributes) {

        let attributeIdPairs = []

        //Get the attributes from the project
        for (let i = 0; i < attributes.length; i += 2) {

            attributeIdPairs.push({
                categoryId: attributes[i],
                attributeId: attributes[i + 1]
            })

        }

        //Populate them with the actual words that match these IDs
        return attributeIdPairs.map(pair => {
            return {
                category: this.words[pair.categoryId],
                attribute: this.words[pair.attributeId]
            }
        })

    }

    getFontStyleValue(fontStyle) {

        switch (fontStyle) {
            case "NORMAL":
                return "normal"
            case "ITALIC":
                return "italic"
            case "BOLD":
                return "normal"
            case "BOLDITALIC":
                return "italic"
        }

    }

    getFontWeightValue(fontStyle) {

        switch (fontStyle) {
            case "NORMAL":
                return "400"
            case "ITALIC":
                return "400"
            case "BOLD":
                return "700"
            case "BOLDITALIC":
                return "700"
        }

    }

    getCenter(background) {
        if (background == "Diamond") return "💎"
        return "⚾️"
    }

    getStats(tokenIndex): Stats {
        //@ts-ignore
        return this.allStats[tokenIndex - 1]
    }

    getNumberWithOrdinal(n) {
        var s = ["th", "st", "nd", "rd"],
            v = n % 100;
        return n + (s[(v - 20) % 10] || s[v] || s[0]);
    }

    getMarkup(stats) {

        this.allStats = stats

        const urlParams = new URLSearchParams(window.location.search)
    
        const tokenId = new BN(urlParams.get('t'), 10) 
        const tokenIndex = tokenId.maskn(128).toString()

        //Get attribute pairs
        let attributePairs = this.getAttributePairs(urlParams.get('a').split(',').map(x => parseInt(x)))

        //Set from attributes
        let theme = attributePairs.filter(pair => pair.category == "Theme")[0].attribute
        let fontFace = attributePairs.filter(pair => pair.category == "Font Face")[0].attribute
        let fontSize = parseInt(attributePairs.filter(pair => pair.category == "Font Size")[0].attribute)
        let fontStyle  = attributePairs.filter(pair => pair.category == "Font Style")[0].attribute

        let colors:Colors = this.getColors(theme)

        let fontStyleValue = this.getFontStyleValue(fontStyle)
        let fontWeightValue = this.getFontWeightValue(fontStyle)

        let center = this.getCenter(theme)

        let tokenStats:Stats = this.getStats(tokenIndex)

        let baserunners = (parseInt(tokenStats.rbi) - 1)

        let menOnBase = ""

        for (let i=0; i < baserunners; i++) {
            menOnBase += "👨"
        }

        const sheet = document.styleSheets[0]

        //Lines
        sheet.insertRule(this.getWordCssRule(theme, colors))


        sheet.insertRule(this.getTextCssRule(theme, colors))

        return `
            <main>
                <div class="word" style="font-family:${fontFace}; font-style: ${fontStyleValue}; font-weight: ${fontWeightValue};">
                                    
                    <div class="text">
                        <div class="padding">
                            <span class="header"><strong>#${tokenIndex}</strong></span>
                            <span class="date">
                                ${tokenStats.date} 
                            </span> <br />

                            <span class="label">Season:</span> #${tokenStats.yearTotal}<br />
            
                            ${baserunners > 0 ? `<span class="label">Baserunners:</span> ${menOnBase}`: ''}

                        </div>
                    </div>

                    <div class="homerun" style=" font-size: ${fontSize}px;">
                        ${ center }
                    </div>

                </div>
            </main>
        `
    }

}

export { WordService }