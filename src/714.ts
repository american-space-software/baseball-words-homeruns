import "reflect-metadata"


//Import CSS
import './html/css/app.css'

import stats from "./data/714"
import { WordService } from "./service/word-service";


let wordService:WordService = new WordService()

export default async () => {
    document.body.innerHTML = wordService.getMarkup(stats)
}

