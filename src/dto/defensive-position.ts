import { Coordinates } from "./coordinates"

class DefensivePosition {

    coordinates:Coordinates
    positionNumber:number

    constructor(positionNumber) {
        this.positionNumber = positionNumber
    }
}

export {
    DefensivePosition
}