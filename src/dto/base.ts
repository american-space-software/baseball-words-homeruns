import { Coordinates } from "./coordinates"

interface Base {

    coordinates:Coordinates
    baseNumber:number

}

export {
    Base
}