
interface Colors {
    color1:string
    color2:string
    fontColor:string
    borderColor:string
    textBackground:string
}

export {
    Colors
}