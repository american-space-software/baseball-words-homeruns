
interface Player {

    color1: string
    color2: string
    jerseyNumber: number

    battingHand?: string 
    pitchingHand?: string

}

export {
    Player
}