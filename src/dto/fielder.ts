import { Coordinates } from "./coordinates"
import { DefensivePosition } from "./defensive-position"
import { Player } from "./player"

interface Fielder {

    coordinates?:Coordinates
    position?:DefensivePosition 
    player:Player 
    
}


export {
    Fielder
}