import { Coordinates } from "./coordinates"
import { Player } from "./player"

interface Runner {

    coordinates?:Coordinates
    player:Player 
    running?:boolean

    baseNumber?:number
    toBase?:number
}

export {
    Runner
}