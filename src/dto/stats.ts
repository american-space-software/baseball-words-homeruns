interface Stats {
    battingOrder:string
    careerTotal:string
    date:string
    gameTotal:string
    home:string
    inning:string
    inningTop:string
    opp:string
    out:string
    rbi:string
    team:string
    wpa:string
    yearTotal:string
    awayTeam:string
    homeTeam:string
} 

export {
    Stats
}