//@ts-nocheck

let table = document.getElementById("play_by_play")

let rows = Array.from(table.rows)

let hr = []

rows.forEach( row => {

    let cells = Array.from(row.cells).map( cell => cell.innerHTML)



    if (cells[1] != "#car") {

        let home = cells[6] == "@" ? false : true

        let homeTeam = home ? cells[5] : cells[7]
        let awayTeam = home ? cells[7] : cells[5]
    
        let dateCell = cells[4].match("<a[^>]*>(.+?)</a>")[1]
        let dateMatches = dateCell.match(/(.*)\((.*?)\)/)

        let date 
        let gameNumber

        if (dateMatches) { //If no matches just use it as the date
            date = new Date(`${dateMatches[1].trim()} EDT`)
            gameNumber = dateMatches[2]
        } else {
            date = new Date(`${dateCell} EDT`)
        }
    
        hr.push({
            careerTotal: cells[1],
            yearTotal: cells[2],
            // gameTotal: cells[3],
            date: `${dateCell.trim()}`,
            team: cells[5],
            opp: cells[7],
            inning: cells[10].match(/\d+/g)[0],
            inningTop: cells[10].includes('t'),
            // out: cells[11],
            rbi: cells[14],
            // battingOrder: cells[14],
            // wpa: cells[16],
            homeTeam: homeTeam,
            awayTeam: awayTeam
        })
    }

})

