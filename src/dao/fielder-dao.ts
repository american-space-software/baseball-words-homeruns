import { injectable } from "inversify";
import { Fielder } from "../dto/fielder";

@injectable()
class FielderDao {
    
    fielders:Fielder[]

    constructor() {
        this.fielders = []
    }

}

export {
    FielderDao
}