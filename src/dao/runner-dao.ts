import { injectable } from "inversify";
import { Runner } from "../dto/runner";

@injectable()
class RunnerDao {
    
    runners:Runner[]
    
    constructor() {
        this.runners = []
    }

}

export {
    RunnerDao
}