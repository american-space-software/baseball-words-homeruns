import { Container } from "inversify";


let container 


let getContainer = () => {

    if (container) return container

    container = new Container()

    
    
    // container.bind(_714Controller).toSelf().inSingletonScope()
    // container.bind(SketchService).toSelf().inSingletonScope()

    return container
}



export {
    getContainer
}