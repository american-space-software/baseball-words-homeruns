import { merge } from 'webpack-merge'
import path from 'path'
import configs from './webpack.common'


let mainConfigs = []

for (let config of configs) {
    //@ts-ignore
    mainConfigs.push(merge(config, {
        //@ts-ignore
        mode: 'development',
        //@ts-ignore
        devtool: 'source-map',

    }))
}

mainConfigs[0].devServer = {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 8082,
    injectClient: false
}


export default mainConfigs



// let mainConfig = merge(common[0], {
//     //@ts-ignore
//     mode: 'development',
//     //@ts-ignore
//     devtool: 'source-map',
//     devServer: {
//         contentBase: path.join(__dirname, 'public'),
//         compress: true,
//         port: 8082,
//         injectClient: false
//     }
// })


// export default [mainConfig]
