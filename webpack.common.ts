import path from 'path'

const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlInlineScriptPlugin = require('html-inline-script-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');
const exec = require('child_process').exec;


let _714Config = {
  entry: './src/714.ts',
  mode: "production",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2|otf)$/,
        use: ['base64-inline-loader']
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  optimization: {
    minimizer: [new TerserPlugin({ extractComments: false })],
  },
  output: {
    filename: '714.js',
    library: "_main",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [

    new CleanWebpackPlugin({
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),

    new HtmlWebpackPlugin({
      title: '714',
      template: 'src/html/index.html',
      filename: '714.html',
      minify: true
    }),

    new HtmlInlineScriptPlugin(),


    // {
    //   apply: (compiler) => {
    //     compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {

    //       exec('ipfs add public/714.html -r', (err, stdout, stderr) => {
    //         if (stdout) process.stdout.write(stdout);
    //         if (stderr) process.stderr.write(stderr);
    //         return
    //       })

    //     })
    //   }
    // }
  ]
}

let _755Config = {
  entry: './src/755.ts',
  mode: "production",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2|otf)$/,
        use: ['base64-inline-loader']
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  optimization: {
    minimizer: [new TerserPlugin({ extractComments: false })],
  },
  output: {
    filename: '755.js',
    library: "_main",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [

    new CleanWebpackPlugin({
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),

    new HtmlWebpackPlugin({
      title: '755',
      template: 'src/html/index.html',
      filename: '755.html',
      minify: true
    }),

    new HtmlInlineScriptPlugin(),


    // {
    //   apply: (compiler) => {
    //     compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {

    //       exec('ipfs add public/755.html -r', (err, stdout, stderr) => {
    //         if (stdout) process.stdout.write(stdout);
    //         if (stderr) process.stderr.write(stderr);
    //         return
    //       })

    //     })
    //   }
    // }
  ]
}

let _762Config = {
  entry: './src/762.ts',
  mode: "production",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2|otf)$/,
        use: ['base64-inline-loader']
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  optimization: {
    minimizer: [new TerserPlugin({ extractComments: false })],
  },
  output: {
    filename: '762.js',
    library: "_main",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [

    new CleanWebpackPlugin({
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),

    new HtmlWebpackPlugin({
      title: '762',
      template: 'src/html/index.html',
      filename: '762.html',
      minify: true
    }),

    new HtmlInlineScriptPlugin(),


    {
      apply: (compiler) => {
        compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {

          exec('ipfs add public -r', (err, stdout, stderr) => {
            if (stdout) process.stdout.write(stdout);
            if (stderr) process.stderr.write(stderr);
            return
          })

        })
      }
    }
  ]
}
export default [_714Config, _755Config, _762Config]





